package aula04;

public class PessoaRica extends Pessoa {
	private double dinheiro;
	
	public void comprar() {
		System.out.println("Fazendo compras...");
	}

	// Sobreescrevendo (overrinding) o m�todo
	public void reclamarCheiro() {
		System.out.println("Que odor desagradavel!");
	}
	
	// Sobreescrevendo e sobrecarregando
	public void reclamarCheiro(int qtd) {
		for(int i=0; i<qtd; i++) {
			reclamarCheiro();
		}
	}
	
	public double getDinheiro() {
		return dinheiro;
	}

	public void setDinheiro(double dinheiro) {
		this.dinheiro = dinheiro;
	}	
}
