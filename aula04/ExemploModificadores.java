package aula04;

import aula04.pacoteA.ClasseModificadores;

public class ExemploModificadores {

	public static void main(String[] args) {
		ClasseModificadores classeModificadores = new ClasseModificadores();
		// Erro: variavelPrivada s� � visivel dentro da ClasseModificadores
		// classeModificadores.variavelPrivada;
		
		// Erro: variavelDefault s� � vis�vel dentro do mesmo pacote
		// classeModificadores.variavelDefault = 123;
	
	    // S� � vis�vel em classe filha	
		//classeModificadores.variavelProtegida = 123;
		
		classeModificadores.variavelPublica = 123;
	}

}
