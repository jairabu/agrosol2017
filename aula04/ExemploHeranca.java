package aula04;

public class ExemploHeranca {

	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setNome("Jair");
		pessoa1.setIdade(33);
		// pessoa1.setDinheiro(1000);
		System.out.println(pessoa1.getNome()+" � Pessoa!");
		
		PessoaRica pessoa2 = new PessoaRica();
		pessoa2.setNome("Jean");
		pessoa2.setIdade(39);		
		pessoa2.setDinheiro(5000);
		pessoa2.comprar();
		System.out.println(pessoa2.getNome()+" � PessoaRica!");
		
		PessoaPobre pessoa3 = new PessoaPobre();
		pessoa3.setNome("Rodolfo (ET)");
		pessoa3.setIdade(35);
		pessoa3.trabalhar();
		System.out.println(pessoa3.getNome()+" � PessoaPobre!");
		
		PessoaMiseravel pessoa4 = new PessoaMiseravel();
		pessoa4.setNome("Mendigata");
		pessoa4.setIdade(26);
		pessoa4.mendigar();
		System.out.println(pessoa4.getNome()+" � PessoaMiseravel!");
		
	}

}
