package aula04;

public class PessoaPobre extends Pessoa {
	public void trabalhar() {		
		System.out.println("Trabalhando... ");
	}
	
	// Sobrepondo (overrinding) o m�todo
	public void reclamarCheiro() {
		// � possivel sobreescrever aproveitando o m�todo pai
		//super.reclamarCheiro();
		System.out.println("Que cheiro de bosta!");
	}
}
