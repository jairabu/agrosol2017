package aula04.pacoteA;

import aula04.pacoteA.ClasseModificadores;

public class ExemploModificadores2 {

	public static void main(String[] args) {
		ClasseModificadores classeModificadores = new ClasseModificadores();
		// Erro: variavelPrivada s� � visivel dentro da ClasseModificadores
		// classeModificadores.variavelPrivada;
		
		classeModificadores.variavelDefault = 123;
		classeModificadores.variavelProtegida = 123;
		classeModificadores.variavelPublica = 123;
	}

}
