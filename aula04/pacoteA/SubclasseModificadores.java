package aula04.pacoteA;

public class SubclasseModificadores extends ClasseModificadores {
	
	public void exemploAcessoProtegido() {
		// Erro: variavelPrivada s� � visivel dentro da ClasseModificadores
		// variavelPrivada = 3;
		
		variavelProtegida = 3;
	}
}
