package aula04;

public class ExemploEnumerador {

	public static void main(String[] args) {
		// Foi utilizado enumerador para as cores pois o sem�foro sempre ter� apenas essas 3 cores!
		CoresEnum cor = CoresEnum.AMARELO;
		
		if(cor == CoresEnum.VERDE) {
			System.out.println("Avan�a...");
		} else if(cor == CoresEnum.AMARELO) {
			System.out.println("Avan�a se estiver na faixa sinalizadora");
		} else if(cor == CoresEnum.VERMELHO) {
			System.out.println("Para!");
		}
	}	
}
