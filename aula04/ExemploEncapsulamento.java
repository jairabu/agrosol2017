package aula04;

public class ExemploEncapsulamento {

	public static void main(String[] args) {		
		// Bad Smell
		// ExemploEncapsulamento est� muito acoplada com a classe ClasseQualquer
		// ClasseQualquer.metodoQualquer(1, "fodsajfs", 123, 321, 321, 321);
				
		Pessoa pessoa1 = new Pessoa();
		//pessoa1.nome = "Jair";
		pessoa1.setNome("Jair");
		System.out.println(pessoa1.getNome());
	}

}
