package aula04;

public class ExemploHerancaMultipla {
	public static void main(String[] args) {
		ProfessorEstrangeiro professor1 = new ProfessorEstrangeiro();
		// Chamando um m�todo fachada
		professor1.setNome("Jair");
		professor1.getProfessor().setSalario(4000);
		professor1.getEstrangeiro().setRne("V104321312W");
		
		// Acessando atributo de Pessoa
		System.out.println("Nome: "+professor1.getProfessor().getNome());
		
		// Acessando atributo de Professor
		System.out.println("Sal�rio: "+professor1.getProfessor().getSalario());
		
		// Acessando atributo de Estrangeiro
		System.out.println("RNE: "+professor1.getEstrangeiro().getRne());
	}
}
