package aula04;

// Java n�o permite heran�a m�ltipla
//public class ProfessorEstrangeiro extends Professor, Estrangeiro {

// "Heran�a m�ltipla" em Java
public class ProfessorEstrangeiro {
	private Professor professor;
	private Estrangeiro estrangeiro;
	
	public ProfessorEstrangeiro() {
		professor = new Professor();
		estrangeiro = new Estrangeiro();
	}
	
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public Estrangeiro getEstrangeiro() {
		return estrangeiro;
	}
	public void setEstrangeiro(Estrangeiro estrangeiro) {
		this.estrangeiro = estrangeiro;
	}
	
	// M�todo Fachada
	public void setNome(String nome) {
		this.getProfessor().setNome(nome);
	}	
}
