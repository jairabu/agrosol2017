package aula04;

public class ExemploSobreescrita {
	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa();
		pessoa1.reclamarCheiro();
		
		PessoaRica pessoa2 = new PessoaRica();
		pessoa2.reclamarCheiro();
		
		PessoaPobre pessoa3 = new PessoaPobre();
		pessoa3.reclamarCheiro();
	}	
}
