package aula08;
import java.util.List;
import java.util.Scanner;

public class Principal {
	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		String opcao;
		do {
			opcao = menu();			
			if(opcao.equals("1")) {
				cadastrarUsuario();
			} else if(opcao.equals("2")) {				
				buscarUsuarioPorCodigo();				
			} else if(opcao.equals("3")) {
				buscarUsuarioPorLogin();							
			} else if(opcao.equals("4")) {
				mostrarUsuarios();
			} else if(opcao.equals("5")) {
				atualizarUsuario();
			} else if(opcao.equals("6")) {
				removerUsuario();
			} else if(!opcao.equals("7")) {
				System.out.println("Op��o inv�lida!");
			}
			
		} while(!opcao.equals("7"));
		
		scanner.close();
	}
	
	public static Usuario buscarPorCodigoOuLogin() {
		System.out.println("1 - Buscar usu�rio por c�digo");
		System.out.println("2 - Buscar usu�rio por login");
		System.out.println("Digite o n�mero da op��o: ");			
		String entrada = scanner.nextLine();
		Usuario u = null;
		if(entrada.equals("1")) {
			u = buscarUsuarioPorCodigo();			
		} else if(entrada.equals("2")) {				
			u = buscarUsuarioPorLogin();			
		}
		return u;
	}
	
	public static void atualizarUsuario() {
		Usuario u = buscarPorCodigoOuLogin();		
		System.out.println("Digite a nova senha:");
		String novaSenha = scanner.nextLine();
		u.setSenha(novaSenha);
		//TODO: UsuarioDAO.update(u);
		System.out.println("Opera��o ainda n�o implementada, contate o suporte!");
	}
	
	public static void removerUsuario() {
		Usuario u = buscarPorCodigoOuLogin();
		System.out.println("1 - Sim");
		System.out.println("2 - N�o");
		System.out.println("Certeza que deseja remover este usuario?");
		if(scanner.nextLine().equals("1")) {
			//TODO: UsuarioDAO.remove(u);
			System.out.println("Opera��o ainda n�o implementada, contate o suporte!");
		}
	}
	
	public static void cadastrarUsuario() {
		try {
			Usuario u = new Usuario();
			System.out.println("Digite o login: ");
			u.setLogin(scanner.nextLine());
			System.out.println("Digite a senha: ");
			u.setSenha(scanner.nextLine());
			UsuarioDAO.create(u);
		} catch(Exception e) {
			System.out.println("Erro ao cadastrar usu�rio: "+e.getMessage());
		}
	}
	
	public static void mostrarUsuarios() {
		try {
			List<Usuario> usuarios = UsuarioDAO.read();
			if(usuarios.isEmpty()) {
				System.out.println("Nenhum usu�rio cadastrado!");
			} else {
				for (Usuario usuario : usuarios) {
					usuario.mostrar();
				}
			}
		} catch(Exception e) {
			System.out.println("Erro ao buscar usu�rios: "+e.getMessage());
		}
	}
	
	public static Long lerCodigo() throws Exception {
		System.out.println("Digite o c�digo do usu�rio: ");
		String entrada = scanner.nextLine();
		Long id = Long.parseLong(entrada);
		return id;
	}
	
	public static String lerLogin() throws Exception {
		System.out.println("Digite o login do usu�rio: ");
		return scanner.nextLine();
	}
	
	public static Usuario buscarUsuarioPorCodigo() {
		Usuario u = null;
		try {
			u = UsuarioDAO.read(lerCodigo());
			u.mostrar();
		} catch(NumberFormatException e) {
			System.out.println("C�digo de usu�rio inv�lido!");
		} catch(Exception e) {
			System.out.println("Erro ao buscar usu�rio: "+e.getMessage());
		}
		return u;
	}
	
	public static Usuario buscarUsuarioPorLogin() {
		Usuario u = null;
		try {
			u = UsuarioDAO.read(lerLogin());
			u.mostrar();
		} catch(Exception e) {
			System.out.println("Erro ao buscar usu�rio: "+e.getMessage());
		}
		return u;
	}
	
	public static String menu() {
		String entrada = "";
		try {			
			System.out.println("1 - Cadastrar usu�rio");
			System.out.println("2 - Buscar usu�rio por c�digo");
			System.out.println("3 - Buscar usu�rio por login");
			System.out.println("4 - Mostrar todos os usu�rios");
			System.out.println("5 - Atualizar dados de usu�rio");
			System.out.println("6 - Remover usu�rio");
			System.out.println("7 - Sair do programa");
			System.out.println("Digite o n�mero da op��o: ");			
			entrada = scanner.nextLine();			
		} catch(Exception e) {
			System.out.println("Erro ao mostrar menu: "+e.getMessage());
		}
		return entrada;
	}
}
