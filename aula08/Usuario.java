package aula08;

public class Usuario {
	private Long id;
	private String login;
	private String senha;
	
	public void mostrar() {
		System.out.printf("Id: %-5d Login: %-30s Senha: %-20s \n", 
				id, login, senha);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}	
}
