package aula08;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionFactory {
	private String driver="org.postgresql.Driver";
	private String url="jdbc:postgresql://localhost/teste";
	private String usuario="postgres";
	private String senha="123";
	
	public Connection getConnection() {
		Connection c = null;		
		try {		
			Class.forName(driver);
			c = DriverManager.getConnection(url, usuario, senha);
		} catch (Exception e) {
			System.out.println("Erro ao conectar no Banco de Dados! "+
					e.getMessage());
			e.printStackTrace();
		}		
		return c;		
	}
}

