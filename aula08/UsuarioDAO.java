package aula08;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

// Classe respons�vel pelo CRUD de Usuario
public class UsuarioDAO {
	public static void create(Usuario u) throws Exception {		 
		/* N�o fa�a isso! Por causa do SQL Injection!
		String sql = "INSERT INTO USUARIO (login, senha) "+
				"VALUES ('"+u.getLogin()+"', '"+u.getSenha()"');"; */			
		String sql = "INSERT INTO USUARIO (login, senha) "+
				"VALUES (?, ?);";
		Connection c = new ConnectionFactory().getConnection();
		PreparedStatement ps = c.prepareStatement(sql);
		ps.setString(1, u.getLogin());
		ps.setString(2, u.getSenha()); // TODO: Criptografar
		ps.executeUpdate();			
		ps.close();
		c.close();		
	}
	
	public static List<Usuario> read() throws Exception {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		
		String sql = "SELECT id, login, senha FROM USUARIO;";
		Connection c = new ConnectionFactory().getConnection();
		PreparedStatement ps = c.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Usuario u = new Usuario();
			u.setId(rs.getLong(1));
			u.setLogin(rs.getString(2));
			u.setSenha(rs.getString(3));
			usuarios.add(u);
		}
		rs.close();
		ps.close();
		c.close();
		
		return usuarios;
	}
	
	public static Usuario read(Long id) throws Exception {
		Usuario u = null;		
		String sql = "SELECT id, login, senha FROM USUARIO "+
				" WHERE id=?;";
		Connection c = new ConnectionFactory().getConnection();
		PreparedStatement ps = c.prepareStatement(sql);
		ps.setLong(1, id);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {				
			u = new Usuario();
			u.setId(rs.getLong(1));
			u.setLogin(rs.getString(2));
			u.setSenha(rs.getString(3));				
		}
		rs.close();
		ps.close();
		c.close();		
		if(u==null) {
			throw new Exception("Usu�rio n�o encontrado");
		}
		return u; 
	}

	public static Usuario read(String login) throws Exception {
		Usuario u = null;
		
		String sql = "SELECT id, login, senha FROM USUARIO "+
				" WHERE login=?;";
		Connection c = new ConnectionFactory().getConnection();
		PreparedStatement ps = c.prepareStatement(sql);
		ps.setString(1, login);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {				
			u = new Usuario();
			u.setId(rs.getLong(1));
			u.setLogin(rs.getString(2));
			u.setSenha(rs.getString(3));				
		}
		rs.close();
		ps.close();
		c.close();
		
		return u; 
	}	
	
	/*
	public static void update(Usuario u) throws Exception {
		
	}
	
	public static void delete(Usuario u) throws Exception {
		
	}*/	
}

