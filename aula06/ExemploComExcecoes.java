package aula06;

public class ExemploComExcecoes {
	public static void main(String[] args) {
		Conta c1 = new Conta();
		c1.setCpf("7366487711");
		c1.setNomeCorrentista("Jair");
		c1.setValor(500);
		
		Conta c2 = new Conta();
		c2.setCpf("1231231231");
		c2.setNomeCorrentista("Waldir");
		c2.setValor(10000);	
				
		// O ideal � criar uma classe para tratar exce��es!
		try {
			// Efetua transfer�ncia com sucesso:
			c1.mostrarDados();
			c2.mostrarDados();
			Conta.transferirComExcecao(c1, c2, 5);
			c1.mostrarDados();
			c2.mostrarDados();
			
			// Lan�a a exce��o e vai pular pro catch
			// As 4 transfer�ncias abaixo n�o ser�o efetuadas!
			Conta.transferirComExcecao(c1, c2, 1000);
			Conta.transferirComExcecao(c1, c2, 10);
			Conta.transferirComExcecao(c1, c2, 20);
			Conta.transferirComExcecao(c1, c2, 30);
		} catch (Exception e) {
			System.out.println("Erro ao efetuar transfer�ncia: "+e.getMessage());
		} finally {
			System.out.println("Fim de opera��o.");
		}
		
	}
}
