package aula06;

public class ContaException extends Exception {
	private String mensagem;
	private String nomeOrigem;
	private String nomeDestino;
	
	public ContaException(String mensagem, String nomeOrigem, String nomeDestino) {
		super(mensagem);
		this.mensagem = mensagem;
		this.nomeOrigem = nomeOrigem;
		this.nomeDestino = nomeDestino;
	}

	public String getMensagem() {
		return mensagem;
	}	

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getNomeOrigem() {
		return nomeOrigem;
	}

	public void setNomeOrigem(String nomeOrigem) {
		this.nomeOrigem = nomeOrigem;
	}

	public String getNomeDestino() {
		return nomeDestino;
	}

	public void setNomeDestino(String nomeDestino) {
		this.nomeDestino = nomeDestino;
	}	
	
}
