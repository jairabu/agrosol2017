package aula06;

public class ExemploHierarquiaExcecoes {
	public static void main(String[] args) {		
		try {
			m1();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void m1() throws Exception {
		System.out.println("m1!");
		m2();
	}
	
	public static void m2() throws Exception {
		System.out.println("m2!");
		m3();
	}
	
	public static void m3() throws Exception {
		System.out.println("m3!");
		throw new Exception("Erro!");
	}
}
