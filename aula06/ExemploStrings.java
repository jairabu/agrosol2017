package aula06;

public class ExemploStrings {

	public static void main(String[] args) {
		String testeA = new String("teste");
		String testeB = new String("teste");
		char[] testeC = {'t','e','s','t','e'};
						
		System.out.println("Tamanho da String testeA: "+testeA.length());
		System.out.println("Obtendo terceiro elemento da String testeA: "+testeA.charAt(2));
		System.out.println("Tudo mai�sculo testeA: "+testeA.toUpperCase());
		
		// O operador == compara refer�ncias (ponteiros, endere�os na mem�ria)
		if(testeA == testeB) {
			System.out.println("Endere�os dos objetos s�o iguais!");
		} else {
			System.out.println("Endere�os dos objetos s�o diferentes!");
		}
		
		if(testeA.equals(testeB)) {
			System.out.println("Conte�do dos objetos s�o iguais!");
		} else {
			System.out.println("Conte�do dos objetos s�o diferentes!");
		}
		
		String frase = "Meu nome � En�ias!";
		String palavras[] = frase.split(" ");
		for (int i = 0; i < palavras.length; i++) {
			System.out.println("Palavra: "+palavras[i]);
		}
		
		// Quest�o de certifica��o Java (pegadinha)
		// Qual a sa�da do c�digo abaixo?
		String testeX = "testando";
		testeX.toUpperCase();
		System.out.println(testeX);
		// (A) Erro de compila��o
		// (B) Erro de execu��o
		// (C) testando
		// (D) TESTANDO
		// (E) Nenhuma das anteiores
		testeX = testeX.toUpperCase();
		System.out.println(testeX);
		
		StringBuffer testesb = new StringBuffer("Teste 123");
		testesb.insert(3, "!");
		testesb.append("concatenar alguma coisa");		
		System.out.println(testesb);
		
		// Vai ocupar um total de 7 caracteres, sendo 2 caracteres depois da v�rgula
		System.out.printf("Nota: %7.2f", 6.1234f);
		
	}

}
