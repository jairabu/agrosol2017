package aula06;

import java.util.Scanner;

public class ExemploChurrasco {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		try {
			System.out.println("Digite a qtd de kilos que teve no churrasco: ");
			float totalKilos = scanner.nextFloat();
			
			System.out.println("Digite a qtd de kilos que voc� comeu no churrasco: ");
			float parteKilos = scanner.nextFloat();
			
			// Antes do Java 8 a divis�o por zero soltava uma exce��o!
			float porcentagem = (parteKilos*100)/totalKilos;
			
			System.out.println("Voc� comeu "+porcentagem+"% do churrasco!");
			
		} catch(Exception e) {
			System.out.println("Erro: "+e.getMessage());
		} finally {
			scanner.close();			
		}
	}
}
