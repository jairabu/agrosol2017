package aula06;

public class Conta {
	private String cpf;
	private String nomeCorrentista;
	private double valor;
	// final: vari�vel constante, ou somente leitura
	// static: vari�vel de classe, compartilhada por todos os objetos
	public static final double LIMITE_TRANSFERENCIA = 2000;
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNomeCorrentista() {
		return nomeCorrentista;
	}
	public void setNomeCorrentista(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	// Exemplo sem exce��o, n�o � seguro!
	public static RetornosTransferencia transferirSemExcecao(Conta contaOrigem, Conta contaDestino, double valor) {
		if(contaOrigem.getValor()<valor) {
			return RetornosTransferencia.SALDO_INSUFICIENTE;
		} else if(valor>Conta.LIMITE_TRANSFERENCIA) {
			return RetornosTransferencia.LIMITE_ULTRAPASSADO;
		}		
		
		contaDestino.setValor(contaDestino.getValor()+valor);
		contaOrigem.setValor(contaOrigem.getValor()-valor);
		
		return RetornosTransferencia.SUCESSO;
	}	
	
	// Exemplo usando exce��o
	public static void transferirComExcecao
		(Conta contaOrigem, Conta contaDestino, double valor) throws Exception {		
		
		if(contaOrigem.getValor()<valor) {
			// Para tudo e resolve esse erro!
			throw new Exception("Saldo insuficiente");			
		} else if(valor>Conta.LIMITE_TRANSFERENCIA) {
			throw new Exception("Limite ultrapassado");			
		}
		
		contaDestino.setValor(contaDestino.getValor()+valor);
		contaOrigem.setValor(contaOrigem.getValor()-valor);
		
		System.out.println("Transfer�ncia de valor "+valor+" efetuada de "+
				contaOrigem.getNomeCorrentista()+" para "+
				contaDestino.getNomeCorrentista()+" com sucesso!");				
	}

	// Exemplo usando exce��o customizada
	public static void transferirComExcecaoCustomizada
		(Conta contaOrigem, Conta contaDestino, double valor) throws ContaException {		
		
		if(contaOrigem.getValor()<valor) {
			// Para tudo e resolve esse erro!
			throw new ContaException("Saldo insuficiente", 
					contaOrigem.getNomeCorrentista(), 
					contaDestino.getNomeCorrentista());			
		} else if(valor>Conta.LIMITE_TRANSFERENCIA) {
			throw new ContaException("Limite ultrapassado",
					contaOrigem.getNomeCorrentista(), 
					contaDestino.getNomeCorrentista());					
		}
		
		contaDestino.setValor(contaDestino.getValor()+valor);
		contaOrigem.setValor(contaOrigem.getValor()-valor);
		
		System.out.println("Transfer�ncia de valor "+valor+" efetuada de "+
				contaOrigem.getNomeCorrentista()+" para "+
				contaDestino.getNomeCorrentista()+" com sucesso!");				
	}
	
	public void mostrarDados() {
		System.out.println("Correntista: "+getNomeCorrentista());
		System.out.println("Saldo: "+getValor());
		System.out.println();
	}
}

enum RetornosTransferencia {
	SUCESSO, SALDO_INSUFICIENTE, LIMITE_ULTRAPASSADO
}

