package aula06;

public class ExemploExcecaoArray {
	public static void main(String[] args) {
		int idades[] = {10, 20, 30};
		
		System.out.println(idades[0]);
		System.out.println(idades[1]);
		System.out.println(idades[2]);
		// A linha abaixo solta exce��o
		// � uma exce��o n�o verificada, ou seja, n�o � obrigat�rio trat�-la!
		// Perigoso pois para a execu��o do programa! (n�o � bom em ambiente Web)		
		//System.out.println(idades[3]);
		// Linha de baixo n�o ser� executada!
		//System.out.println("Outro comando!");
				
		try {
			System.out.println(idades[20]);
		} catch(Exception e) {
			//System.out.println("Deu erro: "+e.getCause());
			e.printStackTrace();
		}
	}
}
