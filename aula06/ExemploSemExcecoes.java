package aula06;

// Vantagem: f�cil de implementar
// Desvantagem: falta de seguran�a
public class ExemploSemExcecoes {
	public static void main(String[] args) {
		Conta c1 = new Conta();
		c1.setCpf("7366487711");
		c1.setNomeCorrentista("Jair");
		c1.setValor(500);
		
		Conta c2 = new Conta();
		c2.setCpf("1231231231");
		c2.setNomeCorrentista("Waldir");
		c2.setValor(10000);
		
		// E se por um acaso o programador esquecer de colocar o if????
		if(Conta.transferirSemExcecao(c1, c2, 1000)==RetornosTransferencia.SUCESSO) {
			System.out.println("Transfer�ncia efetuada com sucesso!");
		} else {
			System.out.println("Erro ao efetuar transfer�ncia!");
		}
		
		// Voc� n�o consegue garantir que o programador n�o ir� fazer isso:
		Conta.transferirSemExcecao(c1, c2, 1000);
	}		
}
