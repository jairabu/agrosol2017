package aula03;

public class ExemploArray {

	public static void main(String[] args) {		
		// int[] idades = new int[5];
		// int idades[] = new int[5];
		
		int[] idades;
		idades = new int[5];
		// Indexando array:
		idades[0] = 0;
		idades[1] = 10;
		idades[2] = 20;
		idades[3] = 30;
		idades[4] = 40;
		// idades[5] = 50; Solta exce��o!
		// Percorrendo array (varrendo array)
		for (int i = 0; i < idades.length; i++) {
			int idade = idades[i];
			System.out.println("Conte�do: "+idade);
		}
		
		// N�o tem efeito no array
		for (int idade : idades) {
			idade = 24;
		}
		
		// For aprimorado � s� para leitura de array
		for (int idade : idades) {
			System.out.println("Conte�do: "+idade);
		}
		
		System.out.println("Soma: "+somar(33));
		System.out.println("Soma: "+somar(2, 4));
		System.out.println("Soma: "+somar(1, 2, 3, 10));
	}
	
	// numeros � um array de tamanho vari�vel (int[] numeros)
	private static double somar(double ... numeros) {
		double soma = 0;
		/*
		for (int i = 0; i < numeros.length; i++) {
			soma+=numeros[i];
		}*/
		for (double i : numeros) {
			soma+=i;
		}
		return soma;
	}

}
