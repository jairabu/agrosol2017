package aula03;

public class Pessoa {
	String nome;
	int idade;
	
	// Construtor
	public Pessoa(String nome, int idade) {
		this.nome = nome;
		this.idade = idade;		
	}
	
	// N�o � um m�todo est�tico, pois ir� manipular dados do objeto!
	public void mostrarDados() {
		System.out.println("Nome: "+nome+" Idade: "+idade);
	}
}
