package aula03;

public class Exercicio09 {

	public static void main(String[] args) {
		MatrizDeInteiros m = new MatrizDeInteiros(2, 3);
		System.out.println("Setando dados na matriz");
		m.setar(10, 1, 24);
		m.setar(1, 10, 24);
		m.setar(0, 0, 24);
		m.setar(1, 2, 33);
		
		System.out.println("\nObtendo dados na matriz");
		System.out.println(m.obter(20, 2));
		System.out.println(m.obter(0, 0));
		System.out.println();
		m.mostrarDados();
		
		if(m.isQuadrada()) {
			System.out.println("Matriz � quadrada!");
		} else {
			System.out.println("Matriz n�o � quadrada!");
		}
		
		System.out.println("A soma dos elementos da matriz � "+m.somarDados());
		
		System.out.println("Procurando linha do elemento de valor 5: "+m.obterLinhaDoElemento(5));
		System.out.println("Procurando linha do elemento de valor 33: "+m.obterLinhaDoElemento(33));
	}

}
