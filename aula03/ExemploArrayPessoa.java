package aula03;

public class ExemploArrayPessoa {

	public static void main(String[] args) {
		Pessoa[] pessoas = new Pessoa[3];
		
		pessoas[0] = new Pessoa("Jair", 33);
		pessoas[1] = new Pessoa("Valdir", 40);
		pessoas[2] = new Pessoa("Jean", 39);
		
		for (Pessoa pessoa : pessoas) {
			pessoa.mostrarDados();
		}
		
		System.out.println();
		pessoas[1].nome = "Waldir";
		for (Pessoa pessoa : pessoas) {
			pessoa.mostrarDados();
		}
	}

}
