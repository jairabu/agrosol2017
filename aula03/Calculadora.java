package aula03;

public class Calculadora {
	// M�todo de inst�ncia (objeto)
	public double somar(double a, double b) {
		double soma = a+b;
		return soma;
	}

	// Sobrecarga (n�o confundir com sobreposi��o)
	// Overload (don't get confused with overriding)
	public int somar(int x, int y) {
		int soma = x+y;
		return soma;
	}
	
	/* Erro, as entradas (argumentos ou parametros) devem ser diferentes!
	public double somar(int a, int b) {
		return 0d;
	} */
	
	
	// M�todo de classe (m�todo est�tico)
	public static double somarEstatico(double a, double b) {
		double soma = a+b;
		return soma;
	}
}
