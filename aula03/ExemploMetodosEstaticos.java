package aula03;

import static java.lang.Math.max;

public class ExemploMetodosEstaticos {
	public static void main(String[] args) {
		Calculadora calculadora = new Calculadora();
		// Chamando m�todo de objeto 
		System.out.println(calculadora.somar(2, 3));
		// Chamando m�todo de classe
		System.out.println(Calculadora.somarEstatico(2, 1));
		
		// N�o � poss�vel chamar um m�todo de objeto pela classe
		// Calculadora.somar(2, 1);
		
		System.out.println(java.lang.Math.subtractExact(10, 2));
		System.out.println(Math.sqrt(16)); // square root
		System.out.println(max(25,	30));
	}
}
