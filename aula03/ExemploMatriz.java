package aula03;

public class ExemploMatriz {
	public static void main(String[] args) {
		char letra = 'J';
		System.out.println(letra);
						
		/*
		char[] letras = new char[5];
		letras[0] = 'T';
		letras[1] = 'e';
		letras[2] = 's';
		letras[3] = 't';
		letras[4] = 'e';
		*/
		
		char[] letras = {'T', 'e', 's', 't', 'e'};
		for (char c : letras) {
			System.out.print(c);
		}
		System.out.println();
		
		/*
		for (int i = 0; i < letras.length; i++) {
			System.out.print(letras[i]);
		}
		System.out.println();*/		
		
		/* System.out.println(String.valueOf(letras)); */
		
		/*
		char[][] tabuleiro = new char[3][3];
		tabuleiro[0][0] = 'O';
		tabuleiro[0][1] = 'O';
		tabuleiro[0][2] = 'X';
		tabuleiro[1][0] = ' ';
		tabuleiro[1][1] = 'X';
		tabuleiro[1][2] = ' ';
		tabuleiro[2][0] = ' ';
		tabuleiro[2][1] = ' ';
		tabuleiro[2][2] = 'X';
		*/
		
		char[][] tabuleiro = {
				{'O', 'O', 'X'},
				{' ', 'X', ' '},
				{' ', ' ', 'X'}
		};
		for (char[] linha : tabuleiro) {
			// Convertendo array de char em String
			System.out.println(String.valueOf(linha));
		}
		
	}
}


