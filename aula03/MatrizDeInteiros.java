package aula03;

public class MatrizDeInteiros {
	private int[][] matriz;
	private int qtdLinhas;
	private int qtdColunas; 
	
	public MatrizDeInteiros(int qtdLinhas, int qtdColunas) {
		matriz = new int[qtdLinhas][qtdColunas];
		this.qtdLinhas = qtdLinhas;
		this.qtdColunas = qtdColunas;
	}
	
	public int obter(int linha, int coluna) {		
		if(isCoordenadaValida(linha, coluna)) {
			return matriz[linha][coluna];
		}
		return -1;
	}
	
	public void setar(int linha, int coluna, int valor) {
		if(isCoordenadaValida(linha, coluna)) {
			matriz[linha][coluna] = valor;
		}		
	}

	private boolean isCoordenadaValida(int linha, int coluna) {		
		if(linha>qtdLinhas || linha<0) {
			System.out.println("Linha inv�lida!");
			return false;
		}
		if(coluna>qtdColunas || coluna < 0) {
			System.out.println("Coluna inv�lida!");
			return false;
		}		
		return true;
	}
	
	public void mostrarDados() {
		for (int linha = 0; linha < qtdLinhas; linha++) {
			for(int coluna = 0; coluna < qtdColunas; coluna++) {
				System.out.printf("%3d ", matriz[linha][coluna]);
			}
			System.out.println();
		}
	}
	
	public boolean isQuadrada() {
		if(qtdLinhas==qtdColunas) {
			return true;
		} else {
			return false;
		}
	}
	
	public int somarDados() {
		int soma = 0;
		for (int linha = 0; linha < qtdLinhas; linha++) {
			for(int coluna = 0; coluna < qtdColunas; coluna++) {
				soma += matriz[linha][coluna];
			}			
		}
		return soma;
	}
	
	public int obterLinhaDoElemento(int valor) {
		for (int linha = 0; linha < qtdLinhas; linha++) {
			for(int coluna = 0; coluna < qtdColunas; coluna++) {
				if(matriz[linha][coluna] == valor) {
					return linha;
				}
			}			
		}
		return -1;
	}
}
