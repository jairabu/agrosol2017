package aula03;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExemploTDD {

	@Test
	public void testAprovado() {
		Aluno aluno1 = new Aluno();
		aluno1.n1 = 10;
		aluno1.n2 = 10;
		aluno1.porcentagemFaltas = 0;
		aluno1.calcularDados();
		assertEquals(true, aluno1.aprovado);
		assertEquals(false, aluno1.reprovadoFaltas);
		assertEquals(false, aluno1.reprovadoNotas);
	}
	
	@Test
	public void testAprovadoNotas() {
		Aluno aluno1 = new Aluno();
		aluno1.n1 = 10;
		aluno1.n2 = 3.4;
		aluno1.porcentagemFaltas = 0;
		aluno1.calcularDados();		
		assertEquals(true, aluno1.aprovado);
		assertEquals(false, aluno1.reprovadoFaltas);
		assertEquals(false, aluno1.reprovadoNotas);
	}
	
	@Test
	public void testReprovadoFaltasENotas() {
		Aluno aluno1 = new Aluno();
		aluno1.n1 = 0;
		aluno1.n2 = 0;
		aluno1.porcentagemFaltas = 100;
		aluno1.calcularDados();
		assertEquals(false, aluno1.aprovado);
		assertEquals(true, aluno1.reprovadoFaltas);
		assertEquals(true, aluno1.reprovadoNotas);
	}
	
	@Test
	public void testReprovadoFaltas() {
		Aluno aluno1 = new Aluno();
		aluno1.n1 = 10;
		aluno1.n2 = 10;
		aluno1.porcentagemFaltas = 25.1d;
		aluno1.calcularDados();
		assertEquals(false, aluno1.aprovado);
		assertEquals(true, aluno1.reprovadoFaltas);
		assertEquals(false, aluno1.reprovadoNotas);
	}
	
	@Test
	public void testReprovadoNotas() {
		Aluno aluno1 = new Aluno();
		aluno1.n1 = 10;
		aluno1.n2 = 3.3;
		aluno1.porcentagemFaltas = 0;
		aluno1.calcularDados();
		assertEquals(false, aluno1.aprovado);
		assertEquals(false, aluno1.reprovadoFaltas);
		assertEquals(true, aluno1.reprovadoNotas);
	}

}
