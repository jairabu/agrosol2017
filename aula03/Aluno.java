package aula03;

public class Aluno {
	String nome;
	double n1;
	double n2;
	double notaFinal;	
	double porcentagemFaltas;
	boolean aprovado;
	boolean reprovadoFaltas;
	boolean reprovadoNotas;
	
	public void calcularDados() {
		notaFinal = 0.4*n1 + 0.6*n2;
		
		if(notaFinal<6) {
			aprovado = false;
			reprovadoNotas = true;
		}
		
		if(porcentagemFaltas>25) {
			aprovado = false;
			reprovadoFaltas = true;
		}
		
		if(notaFinal>=6 && porcentagemFaltas<=25) {
			aprovado = true;
		} 
	}
	
	public void mostrarDados() {
		System.out.println("Aluno: "+nome+" N1: "+n1+" N2:"+n2+" NF:"+notaFinal+
				" % Faltas:"+porcentagemFaltas+(aprovado?" Aprovado":" Reprovado"));
	}
}
