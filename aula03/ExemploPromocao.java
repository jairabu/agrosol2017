package aula03;

public class ExemploPromocao {

	public static void main(String[] args) {
		int inteiro = 24;
		double flutuante = 3.14;
		// Posso jogar (atribuir) um menor no maior (promo��o)
		flutuante = inteiro;
		System.out.println(flutuante);

		// N�o posso jogar um maior no menor
		// inteiro = flutuante;

		// Casting pode ter perda de informa��o!
		flutuante = 1.5;
		inteiro = (int) flutuante;
		System.out.println(inteiro);
				
		// N�o ocorreu promo��o pois houve sobrecarga
		Calculadora calculadora = new Calculadora();
		System.out.println(calculadora.somar(11, 1));
		System.out.println(calculadora.somar(11d, 2d));
		
	}

}
