package aula02;

import java.util.Scanner;

/* C�digo muito fragmentado (granularidade alta) apenas como exemplo! */
public class Exercicio05 {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bem vindo ao troco virtual!");
		System.out.println("Digite a quantidade de moedas de 5 centavos: ");
		int moedasDe5 = scanner.nextInt();
		System.out.println("Digite a quantidade de moedas de 10 centavos: ");
		int moedasDe10 = scanner.nextInt();
		int notasDe2 = calcularNotasDe2Reais(moedasDe5, moedasDe10);
		System.out.println("Quantidade de notas de 2 reais a serem recebidas: "+notasDe2);
		System.out.println("Quantidade de centavos que sobraram: "+
				calcularCentavosSobram(moedasDe5, moedasDe10));
	}
	
	private static int calcularCentavos(int moedasDe5, int moedasDe10) {
		return moedasDe5*5 + moedasDe10*10;
	}
	
	private static int calcularNotasDe2Reais(int moedasDe5, int moedasDe10) {
		//int notasDe2 = (int)(moedasDe5*0.05f+moedasDe10*0.1f);		
		int centavos = calcularCentavos(moedasDe5, moedasDe10);
		int notasDe2 = centavos / 100;
		return notasDe2;
	}
	
	private static int calcularCentavosSobram(int moedasDe5, int moedasDe10) {
		int notasDe2 = calcularNotasDe2Reais(moedasDe5, moedasDe10);
		int totalCentavos = notasDe2*100;
		int centavosSobram = calcularCentavos(moedasDe5, moedasDe10) - totalCentavos; 
		return centavosSobram;
	}

}
