package aula02;

import java.util.Scanner;

public class ExpressaoLogicaComposta {

	public static void main(String[] args) {
		// Exemplo de express�o aritm�tica
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite a N1: ");
		float n1 = scanner.nextFloat();
		System.out.println("Digite a N2: ");
		float n2 = scanner.nextFloat();
		System.out.println("Digite a porcentagem de faltas (sem %):");
		float porcentagemFaltas = scanner.nextFloat();
		float notaFinal = (n1+n2)/2;
		System.out.println("Nota final: "+notaFinal);

		// Exemplo de express�o l�gica
		/*
		if(notaFinal>=6 && porcentagemFaltas<25) {
			System.out.println("Aluno aprovado!");
		} else {
			System.out.println("Aluno reprovado!");
		}*/
	
		if(notaFinal<6 || porcentagemFaltas>=25) {
			System.out.println("Aluno reprovado!");
		} else {
			System.out.println("Aluno aprovado!");
		} 
		
		/*
		if(notaFinal>=6) {
			if(porcentagemFaltas<25) {				
				System.out.println("Aluno aprovado!");
			} else {
				System.out.println("Aluno reprovado!");
			}
		} else {
			System.out.println("Aluno reprovado!");			
		} */
		
		if(notaFinal>=9 && porcentagemFaltas<10)      { System.out.println("Aluno nerd!"); } 
		else if(notaFinal>=6 && porcentagemFaltas<25) { System.out.println("Aluno mediano!"); }
		else if(notaFinal>=4 && porcentagemFaltas<50) { System.out.println("Aluno ruim!"); }
		else 										  { System.out.println("Aluno p�ssimo!"); } 
		
		if(notaFinal<6) {
			System.out.println("Reprovado por nota!");
		}
		
		if(porcentagemFaltas>=25) {
			System.out.println("Reprovado por falta!");
		}

	}

}
