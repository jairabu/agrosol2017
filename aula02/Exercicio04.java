package aula02;

import java.util.Scanner;

public class Exercicio04 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Seja bem vindo ao churrascometro!");
		System.out.println("Digite a quantidade de homens no churrasco: ");
		int homens = scanner.nextInt();
		System.out.println("Digite a quantidade de mulheres no churrasco: ");
		int mulheres = scanner.nextInt();
		int picanhaPorHomem = 350; // gramas
		int picanhaPorMulher = 250; // gramas
		int refrigerantePorPessoa = 500; // ml
		float totalPicanha = calcularTotalPicanha(homens, mulheres, picanhaPorHomem, picanhaPorMulher);
		System.out.println("Total de picanha em kg: "+totalPicanha);
		int pessoas = homens + mulheres;
		float totalGarrafas = calcularTotalGarrafas(pessoas, refrigerantePorPessoa);
		System.out.println("Total de garrafas de 2 lt de refrigerante: "+Math.ceil(totalGarrafas));
		if(verificarEspacoInsuficiente(pessoas)) {
			System.out.println("Espa�o insuficiente para o churrasco!");
		}
	}
		
	private static boolean verificarEspacoInsuficiente(int pessoas) {		
		return pessoas>12 ? true : false;
	}

	// Retorna a quantidade total de picanha em kilogramas
	// picanhaHomem e picanhaMulher � quantidade em gramas
	public static float calcularTotalPicanha(int homens, int mulheres, int picanhaHomem, int picanhaMulher) {
		float totalPicanhaGramas = homens*picanhaHomem + mulheres*picanhaMulher;
		float totalPicanhaKilogramas = totalPicanhaGramas / 1000;
		return totalPicanhaKilogramas;
	}

	// Retorna a quantidade total de garrafas de refrigerantes
	private static float calcularTotalGarrafas(int pessoas, int refrigerantePorPessoa) {
		float totalRefrigeranteMl = pessoas*refrigerantePorPessoa;
		float totalRegrigeranteGarrafa = totalRefrigeranteMl/2000;
		return totalRegrigeranteGarrafa; 
	}

}
