package aula02;

import java.util.Scanner;

public class ExpressaoLogica {

	public static void main(String[] args) {
		// Exemplo de express�o aritm�tica
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite a N1: ");
		float n1 = scanner.nextFloat();
		System.out.println("Digite a N2: ");
		float n2 = scanner.nextFloat();
		float notaFinal = (n1+n2)/2;
		System.out.println("Nota final: "+notaFinal);
		
		// Exemplo de express�o l�gica
		if(notaFinal>=6) {
			System.out.println("Aluno aprovado!");
		} else {
			System.out.println("Aluno reprovado!");
		}
	}

}
