package aula02;

import java.util.Scanner;

public class ProgramaInteresseiro {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Voc� � bonita?");
		String bonitaString = scanner.nextLine();
		System.out.println("Voc� � rica?");
		String ricaString = scanner.nextLine();
		
		boolean bonita = bonitaString.equals("sim") ? true : false;
		/*
		boolean bonita;
		if(bonitaString.equals("sim")) {
			bonita = true;
		} else {
			bonita = false;
		}
		*/
		boolean rica = ricaString.equals("sim") ? true : false;
		/*
		boolean rica;
		if(ricaString.equals("sim")) {
			rica = true;
		} else {
			rica = false;
		} */
		if(bonita && rica) {
			System.out.println("Casa comigo!");
		} else if(bonita && !rica) {
			System.out.println("Fica comigo!");
		} else if(!bonita && rica) {
			System.out.println("Empresta dinheiro!");
		} else if(!bonita && !rica) {
			System.out.println("Sai daqui!");
		} 
		
		/*
		if(bonita) {
			if(rica) {
				System.out.println("Casa comigo!");
			} else {
				System.out.println("Fica comigo!");
			}
		} else {
			if(rica) {
				System.out.println("Empresta dinheiro!");
			} else {
				System.out.println("Sai daqui!");
			}
		}*/
		
		if(rica || bonita) {
			System.out.println("Estou interessado em vc...");
		}
	}

}
