package aula02;

public class ProgramaChato {

	public static void main(String[] args) {
		/*
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!");
		System.out.println("Oi!"); */

		for (int i=0; i<10; i++) {
			System.out.println("Oi! "+i);			
		}
		
		/*
		// N�o fa�a isso! Loop infinito:
		for (int i=0; i<4; i=i) {
			System.out.println("Oi!");			
		} */
		
		/*
		int i=0;
		while(i<4) {
			System.out.println("Oi!");
			i++;
		} */
		
		// Faz e depois pergunta (garantir que executa pelo menos uma vez)
		/*
		int i=0;
		do {
			System.out.println("Oi!");
			i++;
		} while(i<4); */
	}

}
