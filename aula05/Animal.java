package aula05;

public abstract class Animal {
	// M�todo com comportamento padr�o para os filhos:
	public void locomover() {
		System.out.println("Animal se locomovendo...");
	}
	
	// Obrigando as subclasses a implementarem o m�todo (sem comportamento padr�o)
	// M�todo abstrato: "Fa�a o que eu digo e n�o fa�a o que fa�o!
	// Definindo O QUE � pra fazer mas n�o COMO � pra fazer!!!
	abstract void comunicar();
}
