package aula05;

public class Anfibio extends Animal {
	@Override
	public void locomover() {
		System.out.println("Anfibio nadando ou correndo...");
	}
	
	@Override
	void comunicar() {
		System.out.println("Quash croac!");
	}
}
