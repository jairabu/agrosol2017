package aula05.exercicio04;

public class Quadrado extends Quadrilatero {
	
	public Quadrado(float lado) {
		super(lado, lado, lado, lado);		
	}

	@Override
	public double calcularArea() {
		return getLadoA()*getLadoB();
	}

}
