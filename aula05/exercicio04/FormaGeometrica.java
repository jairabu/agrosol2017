package aula05.exercicio04;

public interface FormaGeometrica {
	double calcularPerimetro();
	double calcularArea();
}
