package aula05.exercicio04;

public class Circulo implements FormaGeometrica {
	private float raio;

	public Circulo(float raio) {
		this.raio = raio;
	}

	public float getRaio() {
		return raio;
	}

	public void setRaio(float raio) {
		this.raio = raio;
	}

	@Override
	public double calcularPerimetro() {		
		return 2*Math.PI*getRaio();
	}

	@Override
	public double calcularArea() {
		return Math.PI*Math.pow(getRaio(), 2);		
	}
}
