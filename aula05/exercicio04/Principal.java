package aula05.exercicio04;

import java.util.Scanner;

public class Principal {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Digite a quantidade de formas geom�tricas: ");
		int qtdFormas = scanner.nextInt();
		scanner.nextLine(); // bug do Scanner
		FormaGeometrica[] formas = new FormaGeometrica[qtdFormas];
		for (int i = 0; i < formas.length; i++) {
			FormaGeometrica forma = null;
			System.out.println("Qual forma voc� quer calcular?");
			String resposta = scanner.nextLine();
			if(resposta.equals("circulo")) {
				System.out.println("Digite o tamanho do raio: ");
				float raio = scanner.nextFloat();
				forma = new Circulo(raio);				
			} else if(resposta.equals("quadrado")) {
				System.out.println("Digite o tamanho do lado: ");
				float lado = scanner.nextFloat();				
				forma = new Quadrado(lado);
			} else if(resposta.equals("retangulo")) {								
				System.out.println("Digite a base: ");
				float base = scanner.nextFloat();
				System.out.println("Digite altura: ");
				float altura = scanner.nextFloat();
				forma = new Retangulo(base, altura);
			} // TODO: adicionar quadril�tero
			scanner.nextLine(); // bug do Scanner
			formas[i] = forma;
		}
		
		for (int i = 0; i < formas.length; i++) {
			System.out.println(formas[i].calcularArea());
		}
		
		scanner.close();
	}
}
