package aula05.exercicio04;

// Classe concreta � obrigada a implementar todos os m�todos!
// Interface � proibida de implementar m�todos!
// Classe abstrata � opcional implementar ou n�o os m�todos!
public abstract class Quadrilatero implements FormaGeometrica {
	private float ladoA;
	private float ladoB;
	private float ladoC;
	private float ladoD;
			
	public Quadrilatero(float ladoA, float ladoB, float ladoC, float ladoD) {
		this.ladoA = ladoA;
		this.ladoB = ladoB;
		this.ladoC = ladoC;
		this.ladoD = ladoD;
	}
	public float getLadoA() {
		return ladoA;
	}
	public void setLadoA(float ladoA) {
		this.ladoA = ladoA;
	}
	public float getLadoB() {
		return ladoB;
	}
	public void setLadoB(float ladoB) {
		this.ladoB = ladoB;
	}
	public float getLadoC() {
		return ladoC;
	}
	public void setLadoC(float ladoC) {
		this.ladoC = ladoC;
	}
	public float getLadoD() {
		return ladoD;
	}
	public void setLadoD(float ladoD) {
		this.ladoD = ladoD;
	}
	
	public double calcularPerimetro() {
		return ladoA+ladoB+ladoC+ladoD;
	}	
	
}
