package aula05.exercicio04;

public class Retangulo extends Quadrilatero {
	public Retangulo(float base, float altura) {
		super(base, altura, base, altura);
	}

	@Override
	public double calcularArea() { 
		return getLadoA()*getLadoB();
	}

}

