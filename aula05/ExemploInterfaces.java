package aula05;

public class ExemploInterfaces {

	public static void main(String[] args) {
		Passaro p1 = new Passaro();
		Aviao a1 = new Aviao();
		
		ObjetoVoador[] objetosVoadores = new ObjetoVoador[2];
		objetosVoadores[0] = p1;
		objetosVoadores[1] = a1;
		
		voar(objetosVoadores);
	}

	private static void voar(ObjetoVoador[] objetosVoadores) {
		// Exemplo de polimorfismo com sobreescrita (interface)
		for (int i = 0; i < objetosVoadores.length; i++) {
			objetosVoadores[i].voar();
		}
	}
}
