package aula05;

// Classe final n�o permite ter subclasses (filhos)
public final class Peixe extends Animal {
	@Override
	public void locomover() {		
		System.out.println("Peixe nadando...");
	}
	
	@Override
	void comunicar() {
		System.out.println("Glub glub!");
	}
}

// Erro pois n�o � permitido criar uma subclasse a partir de uma classe final!
/*
class Atum extends Peixe {
	
} */