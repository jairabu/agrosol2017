package aula05;

public class ExemploPolimorfismoAnimais {
	public static void main(String[] args) {
		Peixe a1 = new Peixe();
		Anfibio a2 = new Anfibio();
		Passaro a3 = new Passaro();
		
		Animal[] animais = new Animal[3];
		animais[0] = a1;
		animais[1] = a2;
		animais[2] = a3;
		
		locomover(animais);
		
		// Apesar da refer�ncia ser do tipo Animal, ser� invocado o m�todo locomover do tipo Passaro
		Animal a4 = a3;
		a4.locomover();
	}

	// Aten��o: sempre tente programar com o maior grupo (superclasse) poss�vel (nivel mais alto da �rvore)!
	private static void locomover(Animal[] animais) {
		// Exemplo de polimorfismo com sobreescrita (heran�a)
		for (int i = 0; i < animais.length; i++) {
			if(animais[i] instanceof Passaro) {
				System.out.println("Eu sou um passaro!");
			}
			animais[i].locomover();
			animais[i].comunicar();
			System.out.println();			
		}
	}
}
