package aula05;

import aula04.Pessoa;
import aula04.Professor;

public class ExemploPolimorfismo {

	public static void main(String[] args) {
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setIdade(15);
		pessoa1.setNome("Ma�sa");
		
		// Sempre pergunte o tipo do objeto antes de fazer o downcasting!
		// Neste exemplo n�o entrar� no if!		
		if(pessoa1 instanceof Professor) {
			Professor prof = (Professor)pessoa1;
		}
		
		Professor pessoa2 = new Professor();
		pessoa2.setIdade(33);
		pessoa2.setNome("Jair");
		pessoa2.setSalario(4000);
		
		// Erro pois n�o � toda Pessoa que � Professor!
		// pessoa2 = pessoa1;
		// N�o deu erro pois todo Professor � Pessoa
		// pessoa1 = pessoa2;
		
		
		// Duas refer�ncias apontando para o mesmo objeto do tipo Professor!
		pessoa1 = pessoa2;
		//pessoa1.getSalario();
		pessoa2.getSalario();
				
		// Downcasting
		// Polimorfismo n�o altera o objeto apenas a refer�ncia!
		Professor pessoa3 = (Professor)pessoa1;
		System.out.println("Sal�rio: "+pessoa3.getSalario());
	}

}
