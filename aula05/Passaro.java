package aula05;

public class Passaro extends Animal implements ObjetoVoador {
	@Override
	public void locomover() {
		System.out.println("Passaro voando...");
	}
	
	@Override
	void comunicar() {
		System.out.println("Fiu fiu!");
	}

	@Override
	public void voar() {
		locomover();	
	}		
}
