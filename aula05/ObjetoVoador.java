package aula05;

// Define O QUE � feito mas n�o COMO � feito!
// Interface � um padr�o de "encaixe", ou seja, um conjunto de regras
// Poder�o existir os clientes (consumidores) e os implementadores (produtores)
public interface ObjetoVoador {
	void voar();
}
