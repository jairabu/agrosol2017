package aula01;

public class Pessoa {
	public String nome;
	public int idade;
	
	public Pessoa() {
		idade = 18;
	}
	
	public void mostrarDados() {
		System.out.println("A pessoa "+nome+
				" tem "+idade+" anos!");
	}
}
