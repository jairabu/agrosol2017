package aula01;

public class PessoaTestador {
	public static void main(String[] args) {
		Pessoa p1 = new Pessoa();
		p1.nome = "Douglas";
		//p1.idade = 24;
		p1.mostrarDados();
		
		Pessoa p2 = new Pessoa();
		p2.nome = "Jair";
		p2.idade = 33;
		p2.mostrarDados();
		
		Pessoa p3 = new Pessoa();
		p3.nome = "Jean";
		p3.idade = 39;		
		p3.mostrarDados();
	}
}
